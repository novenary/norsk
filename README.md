# novie's obvious, reduced split keyboard[^naming_scheme][^reduced]

This is a custom ergonomic split keyboard for my personal use.
Documentation will be added soon.
(lol)

- rev1 is a 66 key keyboard.
  I did not like it very much.
- rev2 is the 72 key version that I like a lot better.
- nomsky is a 34 key keyboard.
  It was built from spare parts as an experiment.

This entire repository is released under CC0.
See the LICENSE file for details.

[^naming_scheme]: [Cactus's Obvious, Intuitive Naming Scheme][coins]

[^reduced]:
    "Reduced" is a jab at [modern RISC][risc]:
    despite doing away with many keys I do not consider important,
    this design does not actually pursue minimalism,
    unlike some popular ergonomic keyboards.

    Also, it makes for a funny acronym.

[coins]: https://www.boringcactus.com/2021/03/21/coins.html
[risc]: https://erikmcclure.com/blog/risc-is-fundamentally-unscalable/
