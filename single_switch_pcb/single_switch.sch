EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Switch:SW_Push SW1
U 1 1 6037DB1F
P 3050 3450
F 0 "SW1" H 3050 3735 50  0000 C CNN
F 1 "SW_Push" H 3050 3644 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_Plate" H 3050 3650 50  0001 C CNN
F 3 "~" H 3050 3650 50  0001 C CNN
	1    3050 3450
	1    0    0    -1  
$EndComp
$Comp
L Diode:1N4448W D1
U 1 1 6037D9F3
P 3650 3450
F 0 "D1" H 3650 3233 50  0000 C CNN
F 1 "1N4448W" H 3650 3324 50  0000 C CNN
F 2 "single_switch:D_SOD-123" H 3650 3275 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85722/1n4448w.pdf" H 3650 3450 50  0001 C CNN
	1    3650 3450
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Male COL1
U 1 1 60389909
P 4200 3450
F 0 "COL1" H 4172 3382 50  0000 R CNN
F 1 "Conn_01x01_Male" H 4172 3473 50  0000 R CNN
F 2 "single_switch:SolderWirePad_1x01_SMD_2x4mm" H 4200 3450 50  0001 C CNN
F 3 "~" H 4200 3450 50  0001 C CNN
	1    4200 3450
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Male ROW1
U 1 1 603894C2
P 2400 3450
F 0 "ROW1" H 2508 3631 50  0000 C CNN
F 1 "Conn_01x01_Male" H 2508 3540 50  0000 C CNN
F 2 "single_switch:SolderWirePad_1x01_SMD_2x4mm" H 2400 3450 50  0001 C CNN
F 3 "~" H 2400 3450 50  0001 C CNN
	1    2400 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 3450 2850 3450
Wire Wire Line
	3250 3450 3500 3450
Wire Wire Line
	3800 3450 4000 3450
$EndSCHEMATC
